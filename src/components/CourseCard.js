import {Row, Col, Card, Button} from 'react-bootstrap';

export default function CourseCard(){
	return(
		<Row className="my-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				    <Card.Body>
				        <Card.Title>
				            <h2>Intro to JS</h2>
				        </Card.Title>
				        <Card.Subtitle>
				            <h4>Description:</h4>
				        </Card.Subtitle>
				        <Card.Text>
				            This is a sample course offering
				        </Card.Text>
				        <Card.Subtitle>
				            <h5>Price:</h5>
				        </Card.Subtitle>
				        <Card.Text>
				            PhP 40,000
				        </Card.Text>
				        <Button variant="primary">Enroll</Button>
				    </Card.Body>
				</Card>
				</Col>
		</Row>
	)
}